with open("from-paper/genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]

GBF = expand("from-genbank/{acc}.gbf", acc=GB_ACCESSIONS)
GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)
SHEETS = expand("output/{sheet}.csv", sheet=["seqs"])

rule all:
    input: SHEETS + GBF + GBF_FASTA

rule all_gbf:
    input: GBF

rule all_gbf_fasta:
    input: GBF_FASTA

rule make_seqs_sheet:
    output: "output/seqs.csv"
    input: GBF_FASTA
    shell: "python scripts/make_seqs_sheet.py {input} > {output}"

rule download_gbf:
    """Download one GBF text file per GenBank accession."""
    output: "from-genbank/{acc}.gbf"
    shell: "python scripts/download_genbank.py {wildcards.acc} gb > {output}"

rule download_gbf_fa:
    """Download one FASTA file per GenBank accession."""
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_genbank.py {wildcards.acc} fasta > {output}"
