# Data Gathering from MacLeod 2016

Some scripts to aggregate results and metadata from:

Early Antibody Lineage Diversification and Independent Limb Maturation Lead to Broad HIV-1 Neutralization Targeting the Env High-Mannose Patch
MacLeod, Daniel T. Price, Matt A. et al.
Immunity, Volume 44, Issue 5, 1215 - 1226
<https://doi.org/10.1016/j.immuni.2016.04.016>

 * GenBank accession for mAb heavy+light sequences: KU200842–KU200867
   (**NOTE**: this is 12 pairs of observed mAb heavy/light sequences, but also
   the earliest-looking heavy chain sequence (KU200842) and a germline-reverted
   light chain sequence (KU200855) based on the unpaired repertoire NGS data)
 * NCBI BioProject for repertoire NGS data: PRJNA304070 (but no associated
   sequence data)

## Quick Reference

Terms:

 * PC76: Subject
 * PCDN: Lineage (Protocol C, lab code DN)
 * UAID: unique antibody identifier

Three NGS approaches (see Fig S6):

 * 5' RACE
 * VH4-specific amplification
 * amplification for all VH

PCDN IMGT germline gene assignments:

    IGHV4-34*01
    (D unassigned)
    IGHJ5*01
    IGKV3-20*01
    IGKJ1*01

See also:

 * [paper-helper-landais-2017](https://gitlab.com/ressy-group/paper-helpers/paper-helper-landais-2017)
   for PCT64 lineage from [10.1016/j.immuni.2017.11.002](https://doi.org/10.1016/j.immuni.2017.11.002)
 * [Briney 2016 (10.1038/srep23901)](https://doi.org/10.1038/srep23901) for Clonify/AbAnalysis
 * <https://github.com/briney/abanalysis>
 * <https://github.com/briney/clonify-python>
 * <https://github.com/briney/clonify>
