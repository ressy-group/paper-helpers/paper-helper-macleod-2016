#!/usr/bin/env python

"""
Make CSV of repertoire-derived seq attributes from downloaded GenBank files.

There's only one heavy and one light sequence derived from NGS data in GenBank,
where the heavy sequence is an observed one and the light sequence an inferred
one.  The rest of the repertoire data should be in SRA (but aren't).  The other
GenBank entries are the mAb pairs.
"""

import re
import sys
from csv import DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    match = re.match(
        "([A-Z0-9.]+) Homo sapiens isolate ([^ ]+) anti-HIV immunoglobulin "
        "(heavy|light) chain variable region mRNA, partial cds", txt)
    return match.groups()

FIELDS = [
    "Entry",
    "Category",
    "SeqType",
    "Timepoint",
    "Chain",
    "Accession",
    "SeqID",
    "Seq"]

def make_seqs_sheet(fastas):
    all_attrs = []
    for fasta in fastas:
        with open(fasta) as f_in:
            for record in SeqIO.parse(f_in, "fasta"):
                fields = parse_seq_desc(record.description)
                entry = re.sub("-[HKL]C$", "", fields[1])
                attrs = {field: "" for field in FIELDS}
                attrs["Entry"] = entry
                attrs["Accession"] = fields[0]
                attrs["SeqID"] = fields[1]
                attrs["Chain"] = fields[2]
                attrs["Seq"] = str(record.seq)
                attrs["SeqType"] = "Observed"
                if "-UCA" in entry:
                    if attrs["Chain"] == "heavy":
                        attrs["Category"] = "NGS"
                        attrs["Timepoint"] = 16
                    if attrs["Chain"] == "light":
                        attrs["Category"] = "NGS"
                        attrs["SeqType"] = "Inferred"
                elif re.match(".*[0-9]+[A-Z]", fields[1]):
                    attrs["Category"] = "mab"
                    attrs["Timepoint"] = re.sub("[^-]*-([0-9]+)[A-Z]", r"\1", entry)
                all_attrs.append(attrs)
    def sorter(row):
        entry = row["Entry"]
        is_light = row["Chain"] == "light"
        is_mab = row["Category"] == "mab"
        tp = int(row["Timepoint"]) if row["Timepoint"] else 0
        return (is_light, is_mab, tp, entry)
    all_attrs = sorted(all_attrs, key=sorter)
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(all_attrs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
